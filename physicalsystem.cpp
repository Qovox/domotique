#include "physicalsystem.h"

PhysicalSystem::PhysicalSystem(int hours, int minutes,
                               float temperature, QWidget* parent)
    : QWidget(parent)
    , mainLayout(this)
    , apartment("My Apartment", AlarmState::OFF, "1690", "0648368143")
    , hours(hours)
    , minutes(minutes)
    , temperature(temperature)
{
    //Fix the size of the window. The window is then no more resizable
    setFixedSize(630, 800);
    //Timer initialization
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateTime()));
    timer->start(1000);
    buildUI();
}

void PhysicalSystem::updateTime(void)
{
    if (minutes == 59) {
        if (hours == 23) {
            hours = 0;
        } else {
            hours++;
        }
        minutes = 0;
    } else {
        minutes++;
    }

    uiUpdatePhysicalSystem();
    apartment.update(hours, minutes, temperature);
}

void PhysicalSystem::checkTime(int& hours, int& minutes)
{
    if (hours < 0) {
        hours = 0;
    } else if (hours > 23) {
        hours = 23;
    }
    if(minutes < 0) {
        minutes = 0;
    } else if (minutes > 59) {
        minutes = 59;
    }
}

void PhysicalSystem::buildUI(void)
{
    uiUpdatePhysicalSystem();
    physicalSystemLayout.insertSpacing(0, 50);
    physicalSystemLayout.addWidget(new QLabel("--- Physical system ---"));
    physicalSystemLayout.insertSpacing(2, 50);
    physicalSystemLayout.addWidget(new QLabel("Time: "));
    physicalSystemLayout.addWidget(&lTime);
    physicalSystemLayout.insertSpacing(5, 50);
    physicalSystemLayout.addWidget(new QLabel("Temperature: "));
    physicalSystemLayout.addWidget(&lTemperature);
    physicalSystemLayout.insertSpacing(-1, 50);

    uiSetupSpinBox();
    physicalSystemModifierLayout.addWidget(new QLabel("Time modifier: "));
    physicalSystemModifierLayout.addWidget(&timeModifierSpinBox);
    physicalSystemModifierLayout.insertSpacing(3, 50);
    physicalSystemModifierLayout.addWidget(new QLabel("Temperature modifier: "));
    physicalSystemModifierLayout.addWidget(&temperatureModifierSpinBox);

    connect(&timeModifierSpinBox, SIGNAL(valueChanged(int)),this, SLOT(changeTimeModifier(int)));
    connect(&temperatureModifierSpinBox, SIGNAL(valueChanged(double)),this, SLOT(changeTempModifier(double)));

    mainLayout.addLayout(&physicalSystemLayout);
    mainLayout.addLayout(&physicalSystemModifierLayout);
    mainLayout.addWidget(apartment.getWidgetLayout());

    parentLayout.addWidget(&scrollArea);
    mainWidget.setLayout(&mainLayout);
    scrollArea.setWidget(&mainWidget);
    setLayout(&parentLayout);
}

void PhysicalSystem::uiUpdatePhysicalSystem(void)
{
    const QString qsTime = QString("%1:%2")
            .arg(hours,  2, 10, QChar('0'))
            .arg(minutes,  2, 10, QChar('0'));
    const QString qsTemperature= QString("%1°C").arg(temperature);
    lTime.setText(qsTime);
    lTemperature.setText(qsTemperature);
}

void PhysicalSystem::uiSetupSpinBox(void)
{
    timeModifierSpinBox.setRange(10, 60000);
    timeModifierSpinBox.setSingleStep(100);
    timeModifierSpinBox.setSuffix("ms");
    timeModifierSpinBox.setValue(1000);

    temperatureModifierSpinBox.setRange(-25.0, 15.0);
    temperatureModifierSpinBox.setSingleStep(.5);
    temperatureModifierSpinBox.setSuffix("°C");
    temperatureModifierSpinBox.setValue(0);
}

void PhysicalSystem::changeTimeModifier(int value)
{
    timeModifierSpinBox.setValue(value);
    setTimeModifier(value);
}

void PhysicalSystem::changeTempModifier(double value)
{
    temperatureModifierSpinBox.setValue(value);
    setTemperature(15.0 + value);
}

ostream& operator<<(ostream& os, const PhysicalSystem& ps)
{
    os << "Physical system time " << ps.hours << ":" << ps.minutes << "\n";
    os << "Physical system temperature : " << ps.temperature << "\n";
    os << ps.apartment << "\n";

    return os;
}

void PhysicalSystem::setTime(const int hours, const int minutes)
{
    this->hours = hours;
    this->minutes = minutes;
    checkTime(this->hours, this->minutes);
}

void PhysicalSystem::setTimeModifier(const int timeModifier)
{
    timer->setInterval(timeModifier);
}

void PhysicalSystem::setTemperature(const float temperature)
{
    this->temperature = temperature;
}

PhysicalSystem::~PhysicalSystem()
{
    delete timer;
}
