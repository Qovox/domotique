#include "apartment.h"

Apartment::Apartment(string name, AlarmState alarmState,
                     string alarmCode, string phoneNumber)
    : lAlarmState("Alarm: OFF")
    , alarmOnButton("Alarm on")
    , alarmOffButton("Alarm off")
    , sendCodeButton("Send Code")
    , sendSMSButton("Send SMS")
    , allLightOnButton("All Lights on")
    , allLightOffButton("All Lights off")
    , ac(alarmState, alarmCode, phoneNumber)
    , name(name)
    , alarm(alarmState)
    , alarmCode(alarmCode)
    , phoneNumber(phoneNumber)
{
    gbAparmtemnt = new QGroupBox("Apartment");
    setupRooms();
    buildUi();
    connect(&alarmOnButton, SIGNAL(clicked()), this, SLOT(setAlarmOn(void)));
    connect(&alarmOffButton, SIGNAL(clicked()), this, SLOT(setAlarmOff(void)));
    connect(&sendCodeButton, SIGNAL(clicked()), this, SLOT(sendCode(void)));
    connect(&sendSMSButton, SIGNAL(clicked()), this, SLOT(sendSMS(void)));
    connect(&allLightOnButton, SIGNAL(clicked()), this, SLOT(allLightOn(void)));
    connect(&allLightOffButton, SIGNAL(clicked()), this, SLOT(allLightOff(void)));
}

void Apartment::setupRooms()
{
    for (RoomController* rc : ac.getRoomController()) {
        Room* room = new Room(this, rc->getName(), rc);
        rooms.push_back(room);
    }
    rooms.at(0)->getRoomController()->addLightSegment("morning", 10, 0, 10, 30);
    rooms.at(0)->getRoomController()->addLightSegment("evening", 18, 0, 18, 30);
    rooms.at(0)->getRoomController()->addTempSegment("morning", 0, 0, 9, 59, 24);
    rooms.at(0)->getRoomController()->addTempSegment("noon", 10, 0, 13, 30, 22);
    rooms.at(0)->getRoomController()->addTempSegment("afternoon", 13, 31, 17, 59, 21);
    rooms.at(0)->getRoomController()->addTempSegment("evening", 18, 0, 23, 59, 24);
    rooms.at(0)->uiUpdateLightSegment(); rooms.at(0)->uiUpdateTempSegment();
    rooms.at(0)->uiUpdateWindow();

    rooms.at(1)->getRoomController()->addLightSegment("morning", 6, 30, 7, 0);
    rooms.at(1)->getRoomController()->addTempSegment("morning", 6, 0, 7, 15, 24);
    rooms.at(1)->getRoomController()->addWindow();
    rooms.at(1)->fillComboBox();
    rooms.at(1)->uiUpdateLightSegment(); rooms.at(1)->uiUpdateTempSegment();
    rooms.at(1)->uiUpdateWindow();

    rooms.at(2)->getRoomController()->addLightSegment("morning", 6, 45, 7, 15);
    rooms.at(2)->getRoomController()->addTempSegment("morning", 7, 0, 8, 0, 24);
    rooms.at(2)->getRoomController()->addWindow();
    rooms.at(2)->getRoomController()->addWindow();
    rooms.at(2)->fillComboBox();
    rooms.at(2)->uiUpdateLightSegment(); rooms.at(2)->uiUpdateTempSegment();
    rooms.at(2)->uiUpdateWindow();

    rooms.at(3)->getRoomController()->addTempSegment("morning", 6, 30, 7, 15, 26);
    rooms.at(3)->getRoomController()->addWindow();
    rooms.at(3)->getRoomController()->addWindow();
    rooms.at(3)->getRoomController()->addWindow();
    rooms.at(3)->fillComboBox();
    rooms.at(3)->uiUpdateLightSegment(); rooms.at(3)->uiUpdateTempSegment();
    rooms.at(3)->uiUpdateWindow();
}

void Apartment::update(const int hours, const int minutes, const float externalTemperature)
{
    for (Room* room : rooms) {
        room->update(hours, minutes, externalTemperature);
    }
    //ac.newTime(hours, minutes, externalTemperature);
}

void Apartment::buildUi(void)
{
    QString header = QString("").append(name.c_str()).append(" (%1 rooms)").arg(rooms.size());
    lHeader.setText(header);
    vbApartmentLayout.addWidget(&lHeader);
    hbAlarmLayout.addWidget(&lAlarmState);
    hbAlarmLayout.addWidget(&alarmOnButton);
    hbAlarmLayout.addWidget(&alarmOffButton);
    vbApartmentLayout.addLayout(&hbAlarmLayout);
    leCodeNumber.setMaxLength(4);
    hbUnlockAlarmLayout.addWidget(&leCodeNumber);
    hbUnlockAlarmLayout.addWidget(&sendCodeButton);
    leSMSNumber.setMaxLength(10);
    hbUnlockAlarmLayout.addWidget(&leSMSNumber);
    hbUnlockAlarmLayout.addWidget(&sendSMSButton);
    vbApartmentLayout.addLayout(&hbUnlockAlarmLayout);
    hbLightLayout.addWidget(&allLightOnButton);
    hbLightLayout.insertSpacing(2, 30);
    hbLightLayout.addWidget(&allLightOffButton);
    vbApartmentLayout.addLayout(&hbLightLayout);
    int spacingIndex = 5;
    for (Room* room : rooms) {
        vbApartmentLayout.insertSpacing(spacingIndex, 30);
        vbApartmentLayout.addWidget(room->getWidgetLayout());
        spacingIndex += 2;
    }
    gbAparmtemnt->setLayout(&vbApartmentLayout);
}

void Apartment::uiUpdateAlarmLayout(void)
{
    alarm = ac.getAlarmState();
    switch (alarm) {
    case AlarmState::ON: lAlarmState.setText("Alarm: ON"); break;
    case AlarmState::OFF: lAlarmState.setText("Alarm: OFF"); break;
    case AlarmState::TRIGGERED: lAlarmState.setText("Alarm: TRIGGERED"); break;
    case AlarmState::SUSPENDED: lAlarmState.setText("Alarm: SUSPENDED"); break;
    }
}

ostream& operator<<(ostream& os, const Apartment& apartment)
{
    string alarmState = "OFF";
    if (apartment.alarm == AlarmState::ON)
        alarmState = "ON";
    os << "The apartment contains : " << apartment.rooms.size() << " rooms\n";
    os << "The alarm is currently " << alarmState << endl;

    return os;
}

void Apartment::sendCode(void)
{
    ac.newCodeSent(leCodeNumber.text());
    leCodeNumber.setText("");
}

void Apartment::sendSMS(void)
{
    ac.newSMSSent(leSMSNumber.text());
    leSMSNumber.setText("");
}

void Apartment::setAlarmOn(void)
{
    setAlarmState(AlarmState::ON);
}

void Apartment::setAlarmOff(void)
{
    setAlarmState(AlarmState::OFF);
}

void Apartment::allLightOn(void)
{
    ac.allLightOn();
    for (Room* room : rooms) {
        room->uiUpdateLight();
    }
}

void Apartment::allLightOff(void)
{
    ac.allLightOff();
    for (Room* room : rooms) {
        room->uiUpdateLight();
    }
}

QGroupBox* Apartment::getWidgetLayout(void) const
{
    return gbAparmtemnt;
}

AlarmState Apartment::getAlarm(void) const
{
    return alarm;
}

vector<Room*> Apartment::getRooms(void) const
{
    return rooms;
}

void Apartment::setAlarmState(const AlarmState alarmState)
{
    if (alarmState == AlarmState::ON) {
        ac.newAlarmState(AlarmState::ON);
    } else {
        ac.newAlarmState(AlarmState::OFF);
    }
    uiUpdateAlarmLayout();
}

Apartment::~Apartment()
{
    for (Room* room : rooms)
        delete room;
}
