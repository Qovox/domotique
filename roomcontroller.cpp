#include "roomcontroller.h"
#include "apartmentcontroller.h"

RoomController::RoomController(ApartmentController* ac, RoomName name)
    : ac(ac)
    , roomFSM()
    , name(name)
    , lightState(LightState::OFF)
    , heaterState(HeaterState::OFF)
    , detector(false)
    , progTemp(temperature)
{
    roomFSM.start();
    roomFSM.connectToEvent(QStringLiteral("LightOn"), this, &RoomController::turnOnLight);
    roomFSM.connectToEvent(QStringLiteral("LightOff"), this, &RoomController::turnOffLight);
    roomFSM.connectToEvent(QStringLiteral("FadeLight"), this, &RoomController::fadeLight);
    roomFSM.connectToEvent(QStringLiteral("HeaterOn"), this, &RoomController::turnOnHeater);
    roomFSM.connectToEvent(QStringLiteral("HeaterOff"), this, &RoomController::turnOffHeater);
    roomFSM.connectToEvent(QStringLiteral("SuspendHeater"), this, &RoomController::suspendHeater);
    roomFSM.connectToEvent(QStringLiteral("Heat"), this, &RoomController::heat);
    roomFSM.connectToEvent(QStringLiteral("DetectorOn"), this, &RoomController::turnOnDetector);
    roomFSM.connectToEvent(QStringLiteral("DetectorOff"), this, &RoomController::turnOffDetector);
    roomFSM.connectToEvent(QStringLiteral("OpenWindow"), this, &RoomController::windowsOpened);
    roomFSM.connectToEvent(QStringLiteral("CloseWindow"), this, &RoomController::windowsClosed);
}

void RoomController::addLightSegment(const string name,
                                const int startTimeHours, const int startTimeMinutes,
                                const int endTimeHours, const int endTimeMinutes)
{
    lightSegments.push_back(new LightSegment(name, startTimeHours, startTimeMinutes,
                                         endTimeHours, endTimeMinutes));
}

void RoomController::addTempSegment(const string name,
                                const int startTimeHours, const int startTimeMinutes,
                                const int endTimeHours, const int endTimeMinutes,
                                   const float temperature)
{
    tempSegments.push_back(new TempSegment(name, startTimeHours, startTimeMinutes,
                                         endTimeHours, endTimeMinutes,
                                        temperature));
}

void RoomController::hasToHeat(void)
{
    if (currentTempSegment != nullptr) {
        if (temperature < progTemp && heaterState != HeaterState::HEATING) {
            roomFSM.submitEvent("Start_Heating");
        } else if (temperature >= progTemp && heaterState == HeaterState::HEATING){
            roomFSM.submitEvent("Stop_Heating");
        }
    }
}

void RoomController::addWindow(void)
{
    windows.push_back(false);
}

bool RoomController::allWindowsAreClosed(void) const
{
    for (bool window : windows) {
        if (window)
            return false;
    }
    return true;
}

void RoomController::newTime(const int hours, const int minutes, const float externalTemperature)
{
    temperature = externalTemperature;
    hasToHeat();
    checkLightSegment(hours, minutes);
    checkTempSegment(hours, minutes, externalTemperature);
}

void RoomController::checkLightSegment(const int hours, const int minutes)
{
    for (LightSegment* ls : lightSegments) {
        if (ls->checkStart(hours, minutes)) {
            roomFSM.submitEvent("Fade_Light");
            currentLightSegment = ls;
            return;
        }
        if (ls->checkStop(hours, minutes)) {
            roomFSM.submitEvent("Turn_On_Light");
            currentLightSegment = nullptr;
            return;
        }
    }
}

void RoomController::checkTempSegment(const int hours, const int minutes, const float externalTemperature)
{
    for (TempSegment* ts : tempSegments) {
        if (ts->checkStart(hours, minutes)) {
            currentTempSegment = ts;
            progTemp = ts->getTemperature();
            return;
        }
        if (ts->checkStop(hours, minutes)) {
            roomFSM.submitEvent("Stop_Heating");
            currentTempSegment = nullptr;
            return;
        }
    }
}

void RoomController::newLightState(LightState lightState)
{
    if (lightState == LightState::ON)
        roomFSM.submitEvent("Turn_On_Light");
    else if (lightState == LightState::OFF)
        roomFSM.submitEvent("Turn_Off_Light");
}

void RoomController::newHeaterState(HeaterState heaterState)
{
    if (heaterState == HeaterState::ON)
        roomFSM.submitEvent("Turn_On_Regulator");
    else if (heaterState == HeaterState::OFF)
        roomFSM.submitEvent("Turn_Off_Regulator");
}

void RoomController::newDetectorState(bool detectorState)
{
    if (detectorState) {
        roomFSM.submitEvent(QStringLiteral("Turn_On_Detector"));
        ac->submitEvent("Trigger");
    } else
        roomFSM.submitEvent("Turn_Off_Detector");
}

void RoomController::openWindow(const int index)
{
    windows.at(index) = true;
    roomFSM.submitEvent("Open_Window");
    ac->submitEvent("Trigger");
    ac->submitEvent("Suspend_Alarm");
}

void RoomController::closeWindow(const int index)
{
    windows.at(index) = false;
    if (allWindowsAreClosed()) {
        roomFSM.submitEvent("Close_Window");
        ac->submitEvent("Resume_Alarm");
    }
}

void RoomController::turnOnLight(void)
{
    lightState = LightState::ON;
}

void RoomController::turnOffLight(void)
{
    lightState = LightState::OFF;
}

void RoomController::fadeLight(void)
{
    lightState = LightState::FADING;
}

void RoomController::turnOnHeater(void)
{
    heaterState = HeaterState::ON;
}

void RoomController::turnOffHeater(void)
{
    heaterState = HeaterState::OFF;
}

void RoomController::suspendHeater(void)
{
    heaterState = HeaterState::SUSPENDED;
}

void RoomController::heat(void)
{
    heaterState = HeaterState::HEATING;
}

void RoomController::turnOnDetector(void)
{
    detector = true;
}

void RoomController::turnOffDetector(void)
{
    detector = false;
}

void RoomController::windowsOpened(void)
{
    windowsState = true;
}

void RoomController::windowsClosed(void)
{
    windowsState = false;
}

RoomName RoomController::getName(void) const
{
    return name;
}

sc_room* RoomController::getFSM(void)
{
    return &roomFSM;
}

int RoomController::getLightTime(void)
{
    if (currentLightSegment == nullptr)
        return 1;
    return currentLightSegment->getTime();
}

vector<LightSegment*> RoomController::getLightSegment(void)
{
    return lightSegments;
}

vector<TempSegment*> RoomController::getTempSegment(void)
{
    return tempSegments;
}

vector<bool> RoomController::getWindows(void) const
{
    return windows;
}

LightState RoomController::getLightState(void) const
{
    return lightState;
}

HeaterState RoomController::getHeaterState(void) const
{
    return heaterState;
}

float RoomController::getProgTemp(void) const
{
    return progTemp;
}

bool RoomController::getDetectorState(void) const
{
    return detector;
}

bool RoomController::getWindowsState(void) const
{
    return windowsState;
}

void RoomController::setProgrammedTemperature(const float progTemp)
{
    this->progTemp = progTemp;
}

RoomController::~RoomController()
{
    for (LightSegment* lightSegment : lightSegments)
        delete lightSegment;
    for (TempSegment* tempSegment : tempSegments)
        delete tempSegment;
}
