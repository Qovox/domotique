#include "tempsegment.h"

TempSegment::TempSegment(string name,
                         int startTimeHours, int startTimeMinutes,
                         int endTimeHours, int endTimeMinutes,
                         float temperature)
    : name(name)
    , startTimeHours(startTimeHours)
    , startTimeMinutes(startTimeMinutes)
    , endTimeHours(endTimeHours)
    , endTimeMinutes(endTimeMinutes)
    , temperature(temperature)
{

}

bool TempSegment::checkStart(const int hours, const int minutes) const
{
    return startTimeHours == hours && startTimeMinutes == minutes;
}

bool TempSegment::checkStop(const int hours, const int minutes) const
{
    return endTimeHours == hours && endTimeMinutes == minutes;
}

bool TempSegment::operator==(const TempSegment& segment) const
{
    return this->name == segment.name;
}

bool TempSegment::operator!=(const TempSegment& segment) const
{
    return this->getName() != segment.getName();
}

ostream& operator<<(ostream& os, const TempSegment& segment)
{
    os << "Segment's name : " << segment.name << "\n";
    os << "Segment's start time " << segment.startTimeHours << ":" << segment.startTimeMinutes << "\n";
    os << "Segment's end time " << segment.endTimeHours << ":" << segment.endTimeMinutes << "\n";
    os << "Segment's current temperature : " << segment.temperature << "°C" << endl;

    return os;
}

string TempSegment::getName() const
{
    return name;
}

int TempSegment::getTime(void) const
{
    int eth = endTimeHours;
    if (startTimeHours > endTimeHours)
        eth += 24;
    return (eth * 60 + endTimeMinutes) - (startTimeHours * 60 + startTimeMinutes);
}

float TempSegment::getTemperature(void) const
{
    return temperature;
}

QString TempSegment::getDescription()
{
    QString description = QString("%1 - Heats at %2°C from %3:%4 to %5:%6")
            .arg(name.c_str())
            .arg(temperature)
            .arg(startTimeHours,  2, 10, QChar('0'))
            .arg(startTimeMinutes,  2, 10, QChar('0'))
            .arg(endTimeHours,  2, 10, QChar('0'))
            .arg(endTimeMinutes,  2, 10, QChar('0'));
    return description;
}

void TempSegment::setStartTime(const int hours, const int minutes)
{
    startTimeHours = hours;
    startTimeMinutes = minutes;
}

void TempSegment::setEndTime(const int hours, const int minutes)
{
    endTimeHours = hours;
    endTimeMinutes = minutes;
}

void TempSegment::setTemperature(const float temperature)
{
    this->temperature = temperature;
}
