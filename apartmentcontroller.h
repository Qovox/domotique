#ifndef APARTMENTCONTROLLER_H
#define APARTMENTCONTROLLER_H

#include <QWidget>
#include <QApplication>

#include <vector>
#include <array>

#include "roomcontroller.h"
#include "sc_apartment.h"

using std::vector;
using std::array;

enum class AlarmState { ON, OFF, TRIGGERED, SUSPENDED };

class ApartmentController : public QWidget
{
    Q_OBJECT
public:
    //Constructors / Destructor
    ApartmentController(AlarmState alarmState,
                        string alarmCode, string phoneNumber);

    //--- Functions ---
    void setupRooms(void);
    bool checkPhoneNumbers(string phoneNumber);
    void addPhoneNumber(string phoneNumber);
    void removePhoneNumber(string phoneNumber);
    //Functions to handle external signals
    //void newTime(const int hours, const int minutes, const float externalTemperature);
    void newAlarmState(const AlarmState alarmState);
    void newCodeSent(QString code);
    void newSMSSent(QString sms);
    void allLightOn(void);
    void allLightOff(void);
    void submitEvent(string event);

    //Getters / Setters
    vector<RoomController*> getRoomController() const;
    AlarmState getAlarmState() const;

public slots:
    //Functions for the FSM
    void turnOnAlarm(void);
    void turnAlarmBackOn(void);
    void turnOffAlarm(void);
    void triggerAlarm(void);
    void suspendAlarm(void);

private:
    sc_apartment apartmentFSM;
    AlarmState alarmState;
    string alarmCode;
    array<string, 5> registeredPhoneNumber = { { "", "", "", "", "" } };
    vector<RoomController*> rooms = vector<RoomController*>();

};

#endif // APARTMENTCONTROLLER_H
