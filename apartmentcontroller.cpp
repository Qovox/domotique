#include "apartmentcontroller.h"

ApartmentController::ApartmentController(AlarmState alarmState,
                                         string alarmCode, string phoneNumber)
    : apartmentFSM()
    , alarmState(alarmState)
    , alarmCode(alarmCode)
{
    setupRooms();
    addPhoneNumber(phoneNumber);
    apartmentFSM.start();
    apartmentFSM.connectToEvent(QStringLiteral("TurnOnAlarm"), this, &ApartmentController::turnOnAlarm);
    apartmentFSM.connectToEvent(QStringLiteral("TurnOffAlarm"), this, &ApartmentController::turnOffAlarm);
    apartmentFSM.connectToEvent(QStringLiteral("TriggerAlarm"), this, &ApartmentController::triggerAlarm);
    apartmentFSM.connectToEvent(QStringLiteral("StopTrigger"), this, &ApartmentController::turnAlarmBackOn);
    apartmentFSM.connectToEvent(QStringLiteral("SuspendAlarm"), this, &ApartmentController::suspendAlarm);
}

void ApartmentController::setupRooms(void)
{
    for (RoomName rn : roomNames) {
        RoomController* rc = new RoomController(this, rn);
        rooms.push_back(rc);
    }
}

bool ApartmentController::checkPhoneNumbers(string phoneNumber)
{
    for (unsigned int i = 0; i < registeredPhoneNumber.size(); i++) {
        if (registeredPhoneNumber.at(i) == phoneNumber) {
            return true;
        }
    }
    return false;
}

void ApartmentController::addPhoneNumber(string phoneNumber)
{
    if (!checkPhoneNumbers(phoneNumber)) {
        for (unsigned int i = 0; i < registeredPhoneNumber.size(); i++) {
            if (registeredPhoneNumber.at(i) == "") {
                registeredPhoneNumber.at(i) = phoneNumber;
                return;
            }
        }
    }
}

void ApartmentController::removePhoneNumber(string phoneNumber)
{
    for (unsigned int i = 0; i < registeredPhoneNumber.size(); i++) {
        if (registeredPhoneNumber.at(i) == phoneNumber) {
            registeredPhoneNumber.at(i) = "";
        }
    }
}

void ApartmentController::turnOnAlarm(void)
{
    alarmState = AlarmState::ON;
    for (RoomController* rc : rooms) {
        if(rc->getDetectorState()) {
            apartmentFSM.submitEvent("Trigger");
            return;
        }
    }
}

void ApartmentController::turnAlarmBackOn(void)
{
    alarmState = AlarmState::ON;
    //Turn off every room detector after
    for (RoomController* rc : rooms) {
        rc->getFSM()->submitEvent("Turn_Off_Detector");
    }
}

void ApartmentController::turnOffAlarm(void)
{
    alarmState = AlarmState::OFF;
}

void ApartmentController::triggerAlarm(void)
{
    alarmState = AlarmState::TRIGGERED;
}

void ApartmentController::suspendAlarm(void)
{
    alarmState = AlarmState::SUSPENDED;
}

/*
void ApartmentController::newTime(const int hours, const int minutes, const float externalTemperature)
{

}
*/

void ApartmentController::newAlarmState(AlarmState alarmState)
{
    if (alarmState == AlarmState::ON)
        apartmentFSM.submitEvent(QStringLiteral("Turn_On"));
    else
        apartmentFSM.submitEvent(QStringLiteral("Turn_Off"));
}

void ApartmentController::newCodeSent(QString code)
{
    if (alarmCode == code.toStdString())
        apartmentFSM.submitEvent("Stop_Trigger");
}

void ApartmentController::newSMSSent(QString sms)
{
    if(checkPhoneNumbers(sms.toStdString()))
        apartmentFSM.submitEvent("Stop_Trigger");
}

void ApartmentController::allLightOn(void)
{
    for (RoomController* rc : rooms) {
        rc->newLightState(LightState::ON);
    }
}

void ApartmentController::allLightOff(void)
{
    for (RoomController* rc : rooms) {
        rc->newLightState(LightState::OFF);
    }
}

void ApartmentController::submitEvent(string event)
{
    apartmentFSM.submitEvent(event.c_str());
}

vector<RoomController*> ApartmentController::getRoomController(void) const
{
    return rooms;
}

AlarmState ApartmentController::getAlarmState(void) const
{
    return alarmState;
}
