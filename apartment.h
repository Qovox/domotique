#ifndef APARTMENT_H
#define APARTMENT_H

#include <QWidget>
#include <QGroupBox>
#include <QLineEdit>

#include <iostream>
#include <algorithm>
#include <vector>
#include <string>

#include "apartmentcontroller.h"
#include "room.h"

using std::ostream;
using std::vector;
using std::string;

class Apartment : public QWidget
{
    Q_OBJECT
public:
    //Constructors / Destructor
    Apartment(string name, AlarmState alarmState,
              string alarmCode, string phoneNumber);
    ~Apartment();

    //Functions
    void setupRooms();
    void update(const int hours, const int minutes, const float externalTemperature);
    void buildUi(void);
    void uiUpdateAlarmLayout(void);
    friend ostream& operator<<(ostream& os, const Apartment& apartment);

    //Getters / Setters
    QGroupBox* getWidgetLayout(void) const;
    AlarmState getAlarm(void) const;
    vector<Room*> getRooms(void) const;
    void setAlarmState(const AlarmState alarmState);

public slots:
    void setAlarmOn(void);
    void setAlarmOff(void);
    void sendCode(void);
    void sendSMS(void);
    void allLightOn(void);
    void allLightOff(void);

private:
    QGroupBox* gbAparmtemnt;
    QVBoxLayout vbApartmentLayout;
    QHBoxLayout hbAlarmLayout;
    QHBoxLayout hbUnlockAlarmLayout;
    QHBoxLayout hbLightLayout;
    QLabel lHeader;
    QLabel lAlarmState;
    QLineEdit leCodeNumber;
    QLineEdit leSMSNumber;
    QPushButton alarmOnButton;
    QPushButton alarmOffButton;
    QPushButton sendCodeButton;
    QPushButton sendSMSButton;
    QPushButton allLightOnButton;
    QPushButton allLightOffButton;

    ApartmentController ac;
    string name;
    AlarmState alarm;
    string alarmCode;
    string phoneNumber;
    vector<Room*> rooms = vector<Room*>();

};

#endif // APARTMENT_H
