#include "lightsegment.h"

LightSegment::LightSegment(string name,
                         int startTimeHours, int startTimeMinutes,
                         int endTimeHours, int endTimeMinutes)
    : name(name)
    , startTimeHours(startTimeHours)
    , startTimeMinutes(startTimeMinutes)
    , endTimeHours(endTimeHours)
    , endTimeMinutes(endTimeMinutes)
{

}

bool LightSegment::checkStart(const int hours, const int minutes) const
{
    return startTimeHours == hours && startTimeMinutes == minutes;
}

bool LightSegment::checkStop(const int hours, const int minutes) const
{
    return endTimeHours == hours && endTimeMinutes == minutes;
}

bool LightSegment::operator==(const LightSegment& segment) const
{
    return this->name == segment.name;
}

bool LightSegment::operator!=(const LightSegment& segment) const
{
    return this->getName() != segment.getName();
}

ostream& operator<<(ostream& os, const LightSegment& segment)
{
    os << "Segment's name : " << segment.name << "\n";
    os << "Segment's start time " << segment.startTimeHours << ":" << segment.startTimeMinutes << "\n";
    os << "Segment's end time " << segment.endTimeHours << ":" << segment.endTimeMinutes << endl;

    return os;
}

string LightSegment::getName() const
{
    return name;
}

int LightSegment::getTime(void) const
{
    int eth = endTimeHours;
    if (startTimeHours > endTimeHours)
        eth += 24;
    return (eth * 60 + endTimeMinutes) - (startTimeHours * 60 + startTimeMinutes);
}

QString LightSegment::getDescription()
{
    QString description = QString("%1 - Fades light from %2:%3 to %4:%5")
            .arg(name.c_str())
            .arg(startTimeHours,  2, 10, QChar('0'))
            .arg(startTimeMinutes,  2, 10, QChar('0'))
            .arg(endTimeHours,  2, 10, QChar('0'))
            .arg(endTimeMinutes,  2, 10, QChar('0'));
    return description;
}

void LightSegment::setStartTime(const int hours, const int minutes)
{
    startTimeHours = hours;
    startTimeMinutes = minutes;
}

void LightSegment::setEndTime(const int hours, const int minutes)
{
    endTimeHours = hours;
    endTimeMinutes = minutes;
}
