#ifndef ROOM_H
#define ROOM_H

#include <QWidget>
#include <QComboBox>
#include <QGroupBox>
#include <QGridLayout>
#include <QPushButton>
#include <QSpinBox>
#include <QLabel>

#include <iostream>
#include <vector>

#include "roomcontroller.h"

class Apartment;

using std::ostream;
using std::endl;
using std::vector;
using std::string;

const int timeToHeat = 3;

class Room : public QWidget
{
    Q_OBJECT
public:
    //Constructors / Destructor
    Room(Apartment* apartment, RoomName name, RoomController* rc);
    ~Room();

    //Functions
    void fillComboBox(void);
    void update(int hours, int minutes, float externalTemperature);
    void uiUpdateLight(void);
    int processFading();
    void uiUpdateHeater(void);
    void uiUpdateTemperature(void);
    void uiUpdateDetector(void);
    void uiUpdateLightSegment(void);
    void uiUpdateTempSegment(void);
    void uiUpdateWindow(void);
    void updateTemperature(const float externalTemperature);
    bool operator==(const Room& room) const;
    bool operator!=(const Room& room) const;
    friend ostream& operator<<(ostream& os, const Room& room);

    //Getters / Setters
    QGroupBox* getWidgetLayout(void);
    RoomName getName(void) const;
    string getRoomName(void);
    RoomController* getRoomController(void);
    void setDetector(const bool detectorState);
    void setLight(LightState lightState);
    void setHeater(HeaterState heaterState);
    void setTemperature(const float temperature);

public slots:
    void setLightOn(void);
    void setLightOff(void);
    void setHeaterOn(void);
    void setHeaterOff(void);
    void setDetectorOn(void);
    void setDetectorOff(void);
    void openWindow(void);
    void closeWindow(void);
    void changeProgTemp(double value);

private:
    QGroupBox* gbRoom;
    QVBoxLayout vbRoomLayout;
    QHBoxLayout hbLightLayout;
    QGroupBox gbLightSegment;
    QVBoxLayout vbLightSegmentLayout;
    QGroupBox gbTempSegment;
    QVBoxLayout vbTempSegmentLayout;
    QGroupBox gbWindows;
    QVBoxLayout vbWindowsLayout;
    QComboBox cbWindows;
    QHBoxLayout hbHeaterLayout;
    QHBoxLayout hbTempLayout;
    QHBoxLayout hbDetectorLayout;
    QHBoxLayout hbWindowsLayout;
    QDoubleSpinBox progTempModifierSpinBox;
    QLabel lLight;
    QLabel lDetector;
    QLabel lHeater;
    QLabel lCurrentTemp;
    QLabel lProgTemp;
    QPushButton lightOnButton;
    QPushButton lightOffButton;
    QPushButton heaterOnButton;
    QPushButton heaterOffButton;
    QPushButton detectorOnButton;
    QPushButton openWindowButton;
    QPushButton closeWindowButton;

    vector<QLabel*> lWindows = vector<QLabel*>();

    Apartment* apartment;
    RoomController* rc;
    RoomName name;
    LightState lightState = LightState::OFF;
    HeaterState heaterState = HeaterState::ON;

    int timeFading = 0;
    bool detectorState = false;
    float temperature = 20.0;
    int heatingTime = 0;

};

#endif // ROOM_H
