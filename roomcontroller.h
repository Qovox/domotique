#ifndef ROOMCONTROLLER_H
#define ROOMCONTROLLER_H

#include <QWidget>

#include "lightsegment.h"
#include "tempsegment.h"
#include "sc_room.h"

class ApartmentController;

using std::vector;

enum class RoomName { L1, C1, C2, B1 };
//For iterating purposes
const std::array<RoomName,4> roomNames = {RoomName::L1, RoomName::C1, RoomName::C2, RoomName::B1 };
enum class LightState { ON, OFF, FADING };
enum class HeaterState { ON, OFF, HEATING, SUSPENDED };

class RoomController : public QWidget
{
    Q_OBJECT
public:
    //Constructors / Destructor
    RoomController(ApartmentController* ac, RoomName name);
    ~RoomController();

    //--- Functions ---
    void addLightSegment(const string name,
                    const int startTimeHours, const int startTimeMinutes,
                    const int endTimeHours, const int endTimeMinutes);
    void addTempSegment(const string name,
                    const int startTimeHours, const int startTimeMinutes,
                    const int endTimeHours, const int endTimeMinutes,
                        const float temperature);
    void hasToHeat(void);
    void addWindow(void);
    bool allWindowsAreClosed(void) const;

    //Functions to handle external signals
    void newTime(const int hours, const int minutes, const float externalTemperature);
    void checkLightSegment(const int hours, const int minutes);
    void checkTempSegment(const int hours, const int minutes, const float externalTemperature);
    void newLightState(LightState lightState);
    void newHeaterState(HeaterState heaterState);
    void newDetectorState(bool detectorState);
    void openWindow(const int index);
    void closeWindow(const int index);

    //Getters / Setters
    RoomName getName(void) const;
    sc_room* getFSM(void);
    int getLightTime(void);
    vector<LightSegment*> getLightSegment(void);
    vector<TempSegment*> getTempSegment(void);
    LightState getLightState(void) const;
    HeaterState getHeaterState(void) const;
    vector<bool> getWindows(void) const;
    float getProgTemp(void) const;
    bool getDetectorState(void) const;
    bool getWindowsState(void) const;
    void setProgrammedTemperature(const float progTemp);

public slots:
    //Functions for the FSM
    void turnOnLight(void);
    void turnOffLight(void);
    void fadeLight(void);
    void turnOnHeater(void);
    void turnOffHeater(void);
    void suspendHeater(void);
    void heat(void);
    void turnOnDetector(void);
    void turnOffDetector(void);
    void windowsOpened(void);
    void windowsClosed(void);

private:
    ApartmentController* ac = nullptr;
    sc_room roomFSM;
    RoomName name;
    LightSegment* currentLightSegment = nullptr;
    TempSegment* currentTempSegment = nullptr;
    vector<LightSegment*> lightSegments = vector<LightSegment*>();
    vector<TempSegment*> tempSegments = vector<TempSegment*>();
    vector<bool> windows = vector<bool>();
    LightState lightState;
    HeaterState heaterState;

    bool detector;
    float temperature = 20.0;
    float progTemp;
    //true if at least one window is opened, false otherwise
    bool windowsState = false;

};

#endif // ROOMCONTROLLER_H
