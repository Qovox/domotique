#ifndef TEMPSEGMENT_H
#define TEMPSEGMENT_H

#include <QString>

#include <iostream>
#include <algorithm>

using std::ostream;
using std::endl;
using std::string;

class TempSegment
{

public:
    //Constructors / Destructor
    TempSegment(string name,
            int startTimeHours,
            int startTimeMinutes,
            int endTimeHours,
            int endTimeMinutes,
            float temperature);
    ~TempSegment() = default;

    //Functions
    bool checkStart(const int hours, const int minutes) const;
    bool checkStop(const int hours, const int minutes) const;
    bool operator==(const TempSegment& segment) const;
    bool operator!=(const TempSegment& segment) const;
    friend ostream& operator<<(ostream& os, const TempSegment& segment);

    //Getters / Setters
    string getName(void) const;
    int getTime(void) const;
    float getTemperature(void) const;
    QString getDescription(void);
    void setStartTime(const int hours, const int minutes);
    void setEndTime(const int hours, const int minutes);
    void setTemperature(const float temperature);

private:
    string name;

    int startTimeHours;
    int startTimeMinutes;
    int endTimeHours;
    int endTimeMinutes;
    float temperature;

};

#endif // TEMPSEGMENT_H
