#ifndef PHYSICALSYSTEM_H
#define PHYSICALSYSTEM_H

#include <QWidget>
#include <QTimer>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QScrollArea>

#include <iostream>
#include "apartment.h"

using namespace std;

class PhysicalSystem : public QWidget
{
    Q_OBJECT
public:
    //Constructors / Destructor
    PhysicalSystem(int hours = 0, int minutes = 0,
                   float temperature = 15.0, QWidget* parent = nullptr);
    ~PhysicalSystem();

    //Functions
    void checkTime(int& hours, int& minutes);
    void buildUI(void);
    void uiUpdatePhysicalSystem(void);
    void uiSetupSpinBox(void);
    friend ostream& operator<<(ostream& os, const PhysicalSystem& ps);

    //Getters / Setters
    void setTime(const int hours, const int minutes);
    void setTimeModifier(const int timeModifier);
    void setTemperature(const float temperature);

public slots:
    void updateTime(void);
    void changeTimeModifier(int value);
    void changeTempModifier(double value);

private:
    QVBoxLayout parentLayout;
    QScrollArea scrollArea;
    QVBoxLayout mainLayout;
    QWidget mainWidget;
    QHBoxLayout physicalSystemLayout;
    QHBoxLayout physicalSystemModifierLayout;
    QSpinBox timeModifierSpinBox;
    QDoubleSpinBox temperatureModifierSpinBox;
    QLabel lTime;
    QLabel lTemperature;
    QTimer* timer = nullptr;

    Apartment apartment;

    int hours;
    int minutes;
    float temperature;

};

#endif // PHYSICALSYSTEM_H
