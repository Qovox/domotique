#ifndef LIGHTSEGMENT_H
#define LIGHTSEGMENT_H

#include <QString>

#include <iostream>
#include <algorithm>

using std::ostream;
using std::endl;
using std::string;

class LightSegment
{

public:
    //Constructors / Destructor
    LightSegment(string name,
            int startTimeHours = 18, int startTimeMinutes = 0,
            int endTimeHours = 6, int endTimeMinutes = 0);
    ~LightSegment() = default;

    //Functions
    bool checkStart(const int hours, const int minutes) const;
    bool checkStop(const int hours, const int minutes) const;
    bool operator==(const LightSegment& segment) const;
    bool operator!=(const LightSegment& segment) const;
    friend ostream& operator<<(ostream& os, const LightSegment& segment);

    //Getters / Setters
    string getName(void) const;
    int getTime(void) const;
    QString getDescription(void);
    void setStartTime(const int hours, const int minutes);
    void setEndTime(const int hours, const int minutes);

private:
    string name;

    int startTimeHours;
    int startTimeMinutes;
    int endTimeHours;
    int endTimeMinutes;

};

#endif // LIGHTSEGMENT_H
