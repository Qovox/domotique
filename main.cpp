#include <QApplication>
#include "physicalsystem.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    PhysicalSystem ps;
    ps.show();

    return a.exec();
}
