#include "room.h"
#include "apartment.h"

Room::Room(Apartment* apartment, RoomName name, RoomController* rc)
    : gbLightSegment(QString("Light segments"))
    , gbTempSegment(QString("TempSegment"))
    , gbWindows(QString("Windows"))
    , lLight("Light: OFF")
    , lDetector("Detector: OFF")
    , lHeater("Heater: OFF")
    , lProgTemp("Programmed temperature: ")
    , lightOnButton("Turn on Light")
    , lightOffButton("Turn off Light")
    , heaterOnButton("Turn on Heater")
    , heaterOffButton("Turn off Heater")
    , detectorOnButton("Turn on Detector")
    , openWindowButton("Open Window")
    , closeWindowButton("Close Window")
    , apartment(apartment)
    , name(name)
{
    gbRoom = new QGroupBox(QString("Room %1").arg(getRoomName().c_str()));
    this->rc = rc;
    uiUpdateTemperature();

    progTempModifierSpinBox.setRange(-10.0, 30.0);
    progTempModifierSpinBox.setSingleStep(.5);
    progTempModifierSpinBox.setSuffix("°C");
    progTempModifierSpinBox.setValue(temperature);

    connect(&lightOnButton, SIGNAL(clicked()), this, SLOT(setLightOn()));
    connect(&lightOffButton, SIGNAL(clicked()), this, SLOT(setLightOff()));
    connect(&heaterOnButton, SIGNAL(clicked()), this, SLOT(setHeaterOn()));
    connect(&heaterOffButton, SIGNAL(clicked()), this, SLOT(setHeaterOff()));
    connect(&detectorOnButton, SIGNAL(clicked()), this, SLOT(setDetectorOn()));
    connect(&openWindowButton, SIGNAL(clicked()), this, SLOT(openWindow()));
    connect(&closeWindowButton, SIGNAL(clicked()), this, SLOT(closeWindow()));
    connect(&progTempModifierSpinBox, SIGNAL(valueChanged(double)),this, SLOT(changeProgTemp(double)));
}

void Room::fillComboBox(void)
{
    for (unsigned int i = 0; i < rc->getWindows().size(); i++) {
        cbWindows.addItem(QString("%1").arg(i));
        lWindows.push_back(new QLabel(QString("Windows %1: CLOSED").arg(i)));
        vbWindowsLayout.addWidget(lWindows.at(i));
    }
}

void Room::update(int hours, int minutes, float externalTemperature)
{
    updateTemperature(externalTemperature);
    rc->newTime(hours, minutes, temperature);
    uiUpdateLight();
    uiUpdateHeater();
    uiUpdateTemperature();
    uiUpdateDetector();
}

bool Room::operator==(const Room& room) const
{
    return this->name == room.name;
}

bool Room::operator!=(const Room& room) const
{
    return this->name != room.name;
}

void Room::uiUpdateLight(void)
{
    lightState = rc->getLightState();
    switch (lightState) {
    case LightState::ON:
        lLight.setText("Light: ON");
        timeFading = 0;
        break;
    case LightState::OFF:
        lLight.setText("Light: OFF");
        timeFading = 0;
        break;
    case LightState::FADING: lLight.setText(QString("Light: FADING (%1\%)").arg(processFading())); break;
    }
}

int Room::processFading(void)
{
    if (rc->getLightTime() == 1)
        return 100;
    return (int) (((double)timeFading++ / rc->getLightTime()) * 100);
}

void Room::uiUpdateLightSegment(void)
{
    for (LightSegment* ls : rc->getLightSegment()) {
        vbLightSegmentLayout.addWidget(new QLabel(ls->getDescription()));
    }
}

void Room::uiUpdateHeater(void)
{
    heaterState = rc->getHeaterState();
    switch (heaterState) {
    case HeaterState::ON: lHeater.setText("Heater: ON"); break;
    case HeaterState::OFF: lHeater.setText("Heater: OFF"); break;
    case HeaterState::HEATING: lHeater.setText("Heater: HEATING"); break;
    case HeaterState::SUSPENDED: lHeater.setText("Heater: SUSPENDED"); break;
    }
}

void Room::uiUpdateTemperature(void)
{
    progTempModifierSpinBox.setValue(rc->getProgTemp());
    lCurrentTemp.setText(QString("Current temperature: %1").arg(temperature));
}

void Room::uiUpdateTempSegment(void)
{
    for (TempSegment* ts : rc->getTempSegment()) {
        vbTempSegmentLayout.addWidget(new QLabel(ts->getDescription()));
    }
}

void Room::uiUpdateDetector(void)
{
    detectorState = rc->getDetectorState();
    if (detectorState) {
        lDetector.setText("Detector: ON");
    } else {
        lDetector.setText("Detector: OFF");
    }
    apartment->uiUpdateAlarmLayout();
}

void Room::uiUpdateWindow(void)
{
    for (unsigned int i = 0; i < rc->getWindows().size(); i++) {
        string win = "Windows " + std::to_string(i);
        if (rc->getWindows().at(i)) {
            win += ": OPENED";
            lWindows.at(i)->setText(win.c_str());
        } else {
            win += ": CLOSED";
            lWindows.at(i)->setText(win.c_str());
        }
    }
}

void Room::updateTemperature(const float externalTemperature)
{
    heatingTime++;
    if (heatingTime == 3) {
        heatingTime = 0;
        if (heaterState == HeaterState::HEATING) {
            setTemperature(temperature + 1);
        } else {
            if (temperature > externalTemperature) {
                setTemperature(temperature - 0.5);
            } else if (temperature < externalTemperature) {
                setTemperature(temperature + 0.5);
            }
        }
    }
}

ostream& operator<<(ostream& os, const Room& room)
{
    //os << "Room's name : " << room.name << "\n";
    os << "Room's current temperature : " << room.temperature << "°C" << endl;

    return os;
}

QGroupBox* Room::getWidgetLayout(void)
{
    hbLightLayout.addWidget(&lLight);
    hbLightLayout.addWidget(&lightOnButton);
    hbLightLayout.addWidget(&lightOffButton);
    gbLightSegment.setLayout(&vbLightSegmentLayout);
    vbRoomLayout.addLayout(&hbLightLayout);
    vbRoomLayout.addWidget(&gbLightSegment);

    hbHeaterLayout.addWidget(&lHeater);
    hbHeaterLayout.addWidget(&heaterOnButton);
    hbHeaterLayout.addWidget(&heaterOffButton);
    vbRoomLayout.addLayout(&hbHeaterLayout);
    hbTempLayout.addWidget(&lCurrentTemp);
    hbTempLayout.insertSpacing(2, 40);
    hbTempLayout.addWidget(&lProgTemp);
    hbTempLayout.addWidget(&progTempModifierSpinBox);
    vbRoomLayout.addLayout(&hbTempLayout);
    gbTempSegment.setLayout(&vbTempSegmentLayout);
    vbRoomLayout.addWidget(&gbTempSegment);

    hbDetectorLayout.addWidget(&lDetector);
    hbDetectorLayout.addWidget(&detectorOnButton);
    vbRoomLayout.addLayout(&hbDetectorLayout);

    hbWindowsLayout.addWidget(new QLabel("Window n°"));
    hbWindowsLayout.addWidget(&cbWindows);
    hbWindowsLayout.addWidget(&openWindowButton);
    hbWindowsLayout.addWidget(&closeWindowButton);
    vbRoomLayout.addLayout(&hbWindowsLayout);
    gbWindows.setLayout(&vbWindowsLayout);
    vbRoomLayout.addWidget(&gbWindows);

    gbRoom->setLayout(&vbRoomLayout);
    return gbRoom;
}

RoomName Room::getName(void) const
{
    return name;
}

string Room::getRoomName(void)
{
    switch (name) {
    case RoomName::L1: return "L1";
    case RoomName::C1: return "C1";
    case RoomName::C2: return "C2";
    case RoomName::B1: return "B1";
    default:
        return "No name";
    }
}

RoomController* Room::getRoomController(void)
{
    return rc;
}

void Room::setLightOn(void)
{
    setLight(LightState::ON);
}

void Room::setLightOff(void)
{
    setLight(LightState::OFF);
}

void Room::setLight(LightState lightState)
{
    rc->newLightState(lightState);
    uiUpdateLight();
}

void Room::setHeaterOn(void)
{
    setHeater(HeaterState::ON);
}

void Room::setHeaterOff(void)
{
    setHeater(HeaterState::OFF);
}

void Room::setHeater(HeaterState heaterState)
{
    rc->newHeaterState(heaterState);
    uiUpdateHeater();
}

void Room::setDetectorOn(void)
{
    setDetector(true);
}

void Room::setDetectorOff(void)
{
    setDetector(false);
}

void Room::openWindow(void)
{
    if (rc->getWindows().size()) {
        rc->openWindow(cbWindows.currentIndex());
        uiUpdateWindow();
        uiUpdateHeater();
        apartment->uiUpdateAlarmLayout();
    }
}

void Room::closeWindow(void)
{
    if (rc->getWindows().size()) {
        rc->closeWindow(cbWindows.currentIndex());
        uiUpdateWindow();
        uiUpdateHeater();
        apartment->uiUpdateAlarmLayout();
    }

}

void Room::setDetector(const bool detectorState)
{
    rc->newDetectorState(detectorState);
    uiUpdateDetector();
}

void Room::changeProgTemp(double value)
{
    progTempModifierSpinBox.setValue(value);
    rc->setProgrammedTemperature(value);
}

//The current room's temperature
void Room::setTemperature(const float temperature)
{
    this->temperature = temperature;
}

Room::~Room()
{
    //delete roomLayout;
}
